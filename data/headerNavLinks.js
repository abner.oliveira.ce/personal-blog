const headerNavLinks = [
  { href: '/', title: 'Home' },
  { href: '/about', title: 'Sobre' },
  { href: '/posts', title: 'Posts' },
  { href: '/projects', title: 'Projetos' },
  {
    type: 'dropdown',
    title: 'Outros',
    links: [
      { href: '/tags', title: 'Tags' },
      { href: '/tools', title: 'Ferramentas' },
    ],
  },
]

export default headerNavLinks
